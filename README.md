# Projet voisin-sympa

Réaliser une application d'aide/organisation entre voisin.e.s en mode client-serveur en utilisant le framework JS de son choix.

(Disclaimer: les app et la technologie c'est beau, mais pour ce genre de truc faut bien garder en tête la fracture numérique qui existe et qui laisserait à la porte un certain 
nombre de gens. Là l'idée c'est surtout d'avoir un ptit projet intéressant sur lequel bosser, dans la vraie vie un ptit papier scotché dans son hall ou dans la boîte au lettre ça fait complétement le taf)

## Fonctionnalités

Dans le contexte du COVID-19, pourquoi ne pas essayer de faire une petite application pour pouvoir faire jouer la solidarité avec ses voisin.e.s. 
Les fonctionnalités classiques d'une telle application pourraient se regroupées comme tel :
* Authentification
    * Un login via username/password
    * Une inscription (simple)
* Gestion des groupes
    * Créer un groupe de voisin.e.s
    * Afficher les groupes par adresse (ou autour d'une adresse, avec de la géolocalisation)
    * Rejoindre un groupe
* Gestion des aides
    * Créer une demande/proposition d'aide/d'organisation (choix parmis une liste ou sujet libre)
    * S'inscrire/répondre à la demande/proposition (pourquoi pas avoir un nombre de place définie à la création)
    * Afficher les demandes/proposition de son groupe (code couleur pour les demande/proposition/complétée)

Chaque partie peut être abordée de manière indépendante. Il est tout à fait possible de rajouter d'autres fonctionnalités.

## Technos

Côté back, il va falloir faire une API Rest avec la techno que vous voulez.

Si on part sur du Symfony, pourquoi pas utiliser API Platform ( https://api-platform.com/ ) pour faire ça rapidement.

Si on part sur du Node.js on peut soit le faire avec juste Express.js + mysql, ou alors utiliser un ORM type Sequelize, ou encore un framework type Loopback. 
Dans les deux premiers cas, pour l'authentification, partir sur Passport.js

Pour le front utiliser le framework JS sur lequel vous souhaitez vous entrainer, Angular, Vue ou React (pour Vue et React, je recommande la librairie axios pour les requêtes http)